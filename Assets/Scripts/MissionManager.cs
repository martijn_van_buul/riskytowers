﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System.Linq;
using TMPro;
using UnityEngine.UI;

public class MissionManager : MonoBehaviour
{
    public static MissionManager instance;

    [SerializeField]
    RectTransform overviewImage;

    [SerializeField]
    List<MissionObject> allMissions;
    [SerializeField]
    List<float> missionWeigth;
    [SerializeField]
    List<float> difficultyWeight;

    [SerializeField]
    private List<StatModifier> enemyModifiers;
    [SerializeField]
    private List<TowerStatModifier> towerModifiers;

    [SerializeField]
    private List<RectTransform> EnemyGroupParents;

    private List<MissionObject> currentMissions = new List<MissionObject>();

    [SerializeField]
    List<TMP_Text> missionTexts;

    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        GenerateMissions();
        GenerateEnemyInformation();
    }

    public void RemoveOverlay()
    {
        overviewImage.DOAnchorPosY(1080, 0.5f).SetEase(Ease.OutCirc);
    }

    public void ShowOverlay()
    {
        overviewImage.DOAnchorPosY(0, 0.5f).SetEase(Ease.OutCirc);
    }

    public void StartGame()
    {
        overviewImage.DOAnchorPosY(1080, 0.5f).SetEase(Ease.OutCirc);
        StartCoroutine(EnemyManager.instance.SpawnWave(0.5f));
    }

    private void GenerateMissions()
    {
        currentMissions = new List<MissionObject>();

        while(currentMissions.Count < 3) { 

            float randomMissionFloat = Random.Range(0f, missionWeigth.Sum());
            float randomDifficultyFloat = Random.Range(0f, difficultyWeight.Sum());

            if (randomMissionFloat < missionWeigth[0] && !currentMissions.Contains(allMissions[0]))
            {
                currentMissions.Add(allMissions[0]);
            }
            else if (randomMissionFloat < missionWeigth[0] + missionWeigth[1] && !currentMissions.Contains(allMissions[1]))
            {
                currentMissions.Add(allMissions[1]);
            }
            else if (randomMissionFloat < missionWeigth[0] + missionWeigth[1] + missionWeigth[2])
            {
                EnemyStatModifierMission newMission = ScriptableObject.CreateInstance<EnemyStatModifierMission>();
                EnemyStatModifier enemyModifier = ScriptableObject.CreateInstance<EnemyStatModifier>();
                newMission.Description = allMissions[2].Description;

                enemyModifier.Type = (E_ModifierType)Random.Range(0, 2);
                enemyModifier.Variable = (E_EnemyVariables)Random.Range(0, 2);

                if (randomDifficultyFloat < difficultyWeight[0])
                {
                    newMission.Difficulty = E_MissionDifficulty.Easy;
                    enemyModifier.Value = 0.1f;
                }
                else if (randomDifficultyFloat < difficultyWeight[0] + difficultyWeight[1])
                {
                    newMission.Difficulty = E_MissionDifficulty.Challenging;
                    enemyModifier.Value = 0.2f;
                }
                else if (randomDifficultyFloat < difficultyWeight[0] + difficultyWeight[1] + difficultyWeight[2])
                {
                    newMission.Difficulty = E_MissionDifficulty.Dangerous;
                    enemyModifier.Value = 0.3f;
                }
                else if (randomDifficultyFloat < difficultyWeight[0] + difficultyWeight[1] + difficultyWeight[2] + difficultyWeight[3])
                {
                    newMission.Difficulty = E_MissionDifficulty.Deadly;
                    enemyModifier.Value = 0.5f;
                }

                newMission.StatModifier = enemyModifier;

                if (enemyModifier.Type == E_ModifierType.Multiply)
                {
                    enemyModifier.Value++;
                    newMission.Description = string.Format("Multiply the {1} of all enemies by {0}", enemyModifier.Value, enemyModifier.Variable);
                }
                else
                {
                    newMission.Description = string.Format(newMission.Description, enemyModifier.Value, enemyModifier.Variable, enemyModifier.Type);
                }


                currentMissions.Add(newMission);
            }
        }

        for (int i = 0; i < 3; i++)
        {
            missionTexts[i].text = currentMissions[i].Description;
        }
    }

    private void GenerateEnemyInformation()
    {
        Wave currentWave = EnemyManager.instance.GetCurrentWave();

        for (int i = 0; i < 6; i++)
        {
            if (i < currentWave.EnemyGroups.Count)
            {
                EnemyGroupParents[i].gameObject.SetActive(true);
                EnemyGroup currentGroup = currentWave.EnemyGroups[i];
                RectTransform currentInfoGroup = EnemyGroupParents[i];

                for (int j = 0; j < 7; j++)
                {
                    if (j < currentGroup.Enemies.Count)
                    {
                        EnemyData currentEnemy = currentGroup.Enemies[j];
                        currentInfoGroup.GetChild(j + 1).gameObject.SetActive(true);
                        currentInfoGroup.GetChild(j + 1).GetComponent<EnemyInfoHolder>().MyEnemyData = currentEnemy;

                        Image enemySpriteImage = currentInfoGroup.GetChild(j + 1).GetChild(0).GetComponent<Image>();
                        enemySpriteImage.sprite = currentEnemy.EnemySprite;
                        enemySpriteImage.color = currentEnemy.EnemyColor;
                    }
                    else
                        currentInfoGroup.GetChild(j + 1).gameObject.SetActive(false);
                }
            }
            else
                EnemyGroupParents[i].gameObject.SetActive(false);
        }
    }
}
