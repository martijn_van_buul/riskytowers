﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tile : MonoBehaviour
{
    private Tower myTower;

    public void AddTower(Tower tower)
    {
        myTower = tower;
        gameObject.layer = 9;
    }

    public void RemoveTower()
    {
        gameObject.layer = 10;
    }

    public Tower GetTower()
    {
        return myTower;
    }
}
