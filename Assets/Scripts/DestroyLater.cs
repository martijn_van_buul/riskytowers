﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyLater : MonoBehaviour
{
    [SerializeField]
    float delay = 0.5f;

    IEnumerator Start()
    {
        yield return new WaitForSeconds(delay);

        Destroy(gameObject);
    }
}
