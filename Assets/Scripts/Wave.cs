﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum E_EnemyDifficulty
{
    Deadly,
    Dangerous,
    Challenging,
    Easy
}

public class Wave : ScriptableObject
{
    public List<EnemyGroup> EnemyGroups;
    public float TimeBetween = 1;
    public E_EnemyDifficulty Difficulty;

}
