﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathGenerator : MonoBehaviour
{
    List<GameObject> path = new List<GameObject>();

    [SerializeField]
    Color pathTileColor;

    public IEnumerator GeneratePath()
    {
        List<GameObject> tiles = GridGenerator.instance.GetTiles();

        path.Add(tiles[8]);
        path.Add(tiles[18]);
        path.Add(tiles[28]);
        path.Add(tiles[38]);
        path.Add(tiles[48]);
        path.Add(tiles[58]);
        path.Add(tiles[68]);
        path.Add(tiles[67]);
        path.Add(tiles[66]);
        path.Add(tiles[65]);
        path.Add(tiles[64]);
        path.Add(tiles[63]);
        path.Add(tiles[53]);
        path.Add(tiles[43]);
        path.Add(tiles[33]);
        path.Add(tiles[23]);
        path.Add(tiles[22]);
        path.Add(tiles[21]);
        path.Add(tiles[31]);
        path.Add(tiles[41]);
        path.Add(tiles[51]);
        path.Add(tiles[61]);
        path.Add(tiles[71]);
        path.Add(tiles[81]);
        path.Add(tiles[91]);
        path.Add(tiles[101]);


        foreach (GameObject tile in path)
        {
            tile.GetComponent<SpriteRenderer>().color = pathTileColor;
            tile.layer = 9;
        }

        yield return new WaitForSeconds(0.005f);
        EnemyManager.instance.SetPath(path);
    }
}
