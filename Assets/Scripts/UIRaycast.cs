﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UIRaycast : MonoBehaviour
{
    private bool hoveredTower;

    // Start is called before the first frame update
    void Start()
    {
        InputProcessor.instance.onProcessClick += ProcessClick;
    }

    private void Update()
    {
        hoveredTower = false;

           PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
        eventDataCurrentPosition.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
        List<RaycastResult> results = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventDataCurrentPosition, results);

        foreach (RaycastResult hit in results)
        {
            if (hit.gameObject.GetComponentInChildren<UITower>() != null)
            {
                UIManager.instance.HoveredUITower(hit.gameObject.GetComponentInChildren<UITower>());
                hoveredTower = true;
            }

        }

        if(!hoveredTower)
            UIManager.instance.HoveredUITower(null);
    }

    private void ProcessClick(Vector2 position)
    {
        if (Camera.main)
        {
            //Code to be place in a MonoBehaviour with a GraphicRaycaster component
            GraphicRaycaster graphicRaycaster = this.GetComponent<GraphicRaycaster>();

            if (!graphicRaycaster)
                return;

            //Create the PointerEventData with null for the EventSystem
            PointerEventData eventData = new PointerEventData(null);
            //Set required parameters, in this case, mouse position
            eventData.position = position;
            //Create list to receive all results
            List<RaycastResult> results = new List<RaycastResult>();
            //Raycast it
            graphicRaycaster.Raycast(eventData, results);


            foreach (RaycastResult hit in results)
            {
                if (hit.gameObject.GetComponent<UITower>() != null)
                    hit.gameObject.GetComponent<UITower>().StartDragging();
            }
        }
    }
}
