﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageBuffTowerData : TowerData
{
    public float BuffRange;
    public MultiplierObject DamageMultiplier;
}
