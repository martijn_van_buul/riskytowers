﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class TowerManager : MonoBehaviour
{
    public static TowerManager instance;

    [SerializeField]
    GameObject towerPrefab;
    [SerializeField]
    GameObject damageBuffTowerPrefab;

    private List<GameObject> towers = new List<GameObject>();

    public delegate void TowerRemoved(Tower tower);
    public TowerRemoved onTowerRemoved;

    public delegate void TowerAdded(Tower tower);
    public TowerRemoved onTowerAdded;

    public int TowerLimit;

    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        InputProcessor.instance.onProcessRightClick += DeleteTower;
    }

    public bool BuildTower(Tile tile, TowerData data)
    {
        if(towers.Count < TowerLimit && UIManager.instance.goldAmount >= data.Cost)
        {
            UIManager.instance.BuyTower(data.Cost);
            GameObject instantiatiedTower = null;
            Tower newTower = null;

            if (data is DamageBuffTowerData)
                instantiatiedTower = Instantiate(damageBuffTowerPrefab, tile.transform.position, damageBuffTowerPrefab.transform.rotation, transform);
            else if(data is TowerData)
            instantiatiedTower = Instantiate(towerPrefab, tile.transform.position, towerPrefab.transform.rotation, transform);

            newTower = instantiatiedTower.GetComponent<Tower>();
            newTower.Initialise(data);
        
            towers.Add(instantiatiedTower);

            if (onTowerAdded != null)
                onTowerAdded(newTower);

            return true;
        }
        else
            return false;
    }

    private void DeleteTower(Vector2 position)
    {
        if (Camera.main)
        {
            Ray screenPointToPosition = Camera.main.ScreenPointToRay(position);
            RaycastHit[] hits = Physics.RaycastAll(screenPointToPosition.origin, screenPointToPosition.direction, Mathf.Infinity, 1 << 9);

            foreach (RaycastHit rayHit in hits)
            {
                Tile HitTile = rayHit.collider.GetComponent<Tile>();
                if (HitTile != null)
                {
                    Tower towerToBeRemoved = HitTile.GetTower();
                    GameObject removeTowerObject = towerToBeRemoved.gameObject;

                    HitTile.RemoveTower();

                    if (onTowerRemoved != null)
                        onTowerRemoved(towerToBeRemoved);

                    Destroy(removeTowerObject);
                    return;
                }
            }

        }
    }

    public List<GameObject> GetTowersInRange(Vector3 position, float range)
    {
        return towers.Where(t => Vector3.Distance(t.transform.position, position) < range).ToList();
    }
}
