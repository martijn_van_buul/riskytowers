﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using DG.Tweening;

public enum E_TowerVariables
{
    range,
    damage,
    attackspeed,
    cost
}
public class Tower : MonoBehaviour
{
    [SerializeField]
    GameObject bulletPrefab;

    List<Enemy> enemiesInRange = new List<Enemy>();

    public float Range = 5;
    public float Damage = 2;
    public float AttackSpeed = 1;
    public int Cost= 2;
    private float currentCooldown;

    bool canShoot = true;
    Enemy closestEnemy;

    private TowerData data;

    private float damageMultiplier = 1;

    List<MultiplierObject> myMultipliers = new List<MultiplierObject>();

    public virtual void Initialise(TowerData data)
    {
        this.data = data;
        Range = data.Range;
        Damage = data.Damage;
        AttackSpeed = data.AttackSpeed;
        Cost = data.Cost;

        GetComponent<SpriteRenderer>().sprite = data.towerSprite;
        GetComponent<SpriteRenderer>().color = data.towerColor;

        transform.DOPunchScale(Vector3.one * 1f, 0.25f, 5, 0.2f);
    }

    // Update is called once per frame
    protected virtual void Update()
    {
        if (canShoot)
        {
            enemiesInRange.Clear();

            foreach (Enemy enemy in EnemyManager.instance.Enemies)
                if (Vector3.Distance(enemy.transform.position, transform.position) < Range)
                    enemiesInRange.Add(enemy);

            if (enemiesInRange.Count == 0)
                return;

            closestEnemy = enemiesInRange.First(enemyClosest => enemyClosest.pathIndex == enemiesInRange.Max(enemy => enemy.pathIndex));

            ShootEnemy(closestEnemy);

            canShoot = false;
            currentCooldown = 0;
        }
        else
        {
            currentCooldown += Time.deltaTime;

            if (currentCooldown > 1 / AttackSpeed)
                canShoot = true;
        }

        if(closestEnemy != null)
        {
            transform.LookAt(closestEnemy.transform, Vector3.down);
            transform.localEulerAngles = new Vector3(90, transform.localEulerAngles.y, 0);
        }
    }

    protected virtual void ShootEnemy(Enemy target)
    {
        GameObject bullet = Instantiate(bulletPrefab, transform.position + Vector3.forward * 0.01f, bulletPrefab.transform.rotation, transform);
        bullet.GetComponent<BulletBehaviour>().Initialise(target, Damage * damageMultiplier);
        transform.DOKill();
        transform.localScale = Vector3.one;
        transform.DOPunchScale(Vector3.one * 0.35f, 0.1f, 5, 0.2f);
    }

    public void AddDamageMultiplier(MultiplierObject multiplier)
    {
        myMultipliers.Add(multiplier);

        damageMultiplier *= multiplier.Multiplier;
    }
}
