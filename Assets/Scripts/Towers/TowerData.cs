﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerData : ScriptableObject
{
    public float Range = 5;
    public float Damage = 2;
    public float AttackSpeed = 1;
    public int Cost = 2;
    public Sprite towerSprite;
    public Color towerColor;

    public string Description = "";

    public string GetDescription()
    {
        return string.Format(Description, Range, Damage, AttackSpeed, Cost);
    }
}
