﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UITower : MonoBehaviour
{
    bool dragging = false;

    RectTransform myTransform;
    Vector3 initialPositionMouse;
    Vector3 startPosition;

    public TowerData MyData;

    public void initialise(TowerData data)
    {
        MyData = data;
        GetComponent<Image>().sprite = data.towerSprite;
        GetComponent<Image>().color = data.towerColor;
    }

    private void Start()
    {
        myTransform = GetComponent<RectTransform>();
        startPosition = myTransform.anchoredPosition;
    }

    public void StartDragging()
    {
        dragging = true;
        initialPositionMouse = Input.mousePosition;
        InputProcessor.instance.onClickRelease += ClickReleased;
    }

    private void Update()
    {
        if (dragging)
        {
            myTransform.anchoredPosition = startPosition + Input.mousePosition - initialPositionMouse;
        }
    }

    private void ClickReleased(Vector2 position, float duration, float distance)
    {
        dragging = false;
        InputProcessor.instance.onClickRelease -= ClickReleased;

        
        if (Camera.main)
        {
            Ray screenPointToPosition = Camera.main.ScreenPointToRay(position);
            RaycastHit[] hits = Physics.RaycastAll(screenPointToPosition.origin, screenPointToPosition.direction, Mathf.Infinity, 1 << 10);

            foreach (RaycastHit rayHit in hits)
            {
                Tile hitTile = rayHit.collider.GetComponent<Tile>();
                if (hitTile != null && TowerManager.instance.BuildTower(hitTile, MyData))
                {
                    Destroy(gameObject);
                    return;
                }
            }
            
        }

        myTransform.anchoredPosition = startPosition;
    }

}
