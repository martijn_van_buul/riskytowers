﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageBuffTower : Tower
{
    public float BuffRange;
    public MultiplierObject DamageMultiplier;

    public override void Initialise(TowerData data)
    {
        base.Initialise(data);

        this.BuffRange = ((DamageBuffTowerData)data).BuffRange;
        this.DamageMultiplier = ((DamageBuffTowerData)data).DamageMultiplier;

        DamageMultiplier.Source = gameObject;

        foreach (GameObject tower in TowerManager.instance.GetTowersInRange(transform.position, BuffRange))
            tower.GetComponent<Tower>().AddDamageMultiplier(DamageMultiplier);

        TowerManager.instance.onTowerAdded += CheckTowerBuff;
    }

    private void CheckTowerBuff(Tower tower)
    {
        if(Vector3.Distance(transform.position, tower.transform.position) < BuffRange)
            tower.AddDamageMultiplier(DamageMultiplier);
    }
}
