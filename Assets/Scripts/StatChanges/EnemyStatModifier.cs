﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyStatModifier : StatModifier
{
    public E_EnemyVariables Variable;
    public E_ModifierType Type;
    public float Value;
}
