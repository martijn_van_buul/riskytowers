﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class TowerStatModifier : StatModifier
{
    public E_TowerVariables Variable;
    public E_ModifierType Type;
    public float Value;
}
