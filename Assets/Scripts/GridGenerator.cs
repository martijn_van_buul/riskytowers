﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridGenerator : MonoBehaviour
{
    public static GridGenerator instance;


    [SerializeField]
    GameObject tilePrefab;

    List<GameObject> tiles = new List<GameObject>();

    public int GridSize = 10;

    private void Awake()
    {
        instance = this;
    }

    void Start()
    {
        StartCoroutine(GeneratePool());
    }

    public IEnumerator GeneratePool()
    {
        for (int x = 0; x <= GridSize; x++)
        {
            for (int z = 0; z < GridSize; z++)
            {
                GameObject instantiatiedTile = Instantiate(tilePrefab, new Vector3(x, 0, z), tilePrefab.transform.rotation);
                instantiatiedTile.transform.SetParent(transform);
                tiles.Add(instantiatiedTile);
                instantiatiedTile.layer = 10;

            }
        }

        yield return new WaitForSeconds(0.005f);
        StartCoroutine(GetComponent<PathGenerator>().GeneratePath());
    }

    public List<GameObject> GetTiles()
    {
        return tiles;
    }

}
