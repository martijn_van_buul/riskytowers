﻿using UnityEngine;
using System.Collections;
using UnityEditor;

public class MakeScriptableObject
{
    [MenuItem("Assets/Create/New Wave")]
    public static void CreateWave()
    {
        Wave asset = ScriptableObject.CreateInstance<Wave>();

        AssetDatabase.CreateAsset(asset, "Assets/ScriptableObjects/Waves/Wave.asset");
        AssetDatabase.SaveAssets();

        EditorUtility.FocusProjectWindow();

        Selection.activeObject = asset;
    }

    [MenuItem("Assets/Create/New Enemy")]
    public static void CreateEnemyData()
    {
        EnemyData asset = ScriptableObject.CreateInstance<EnemyData>();

        AssetDatabase.CreateAsset(asset, "Assets/ScriptableObjects/Enemies/Enemy.asset");
        AssetDatabase.SaveAssets();

        EditorUtility.FocusProjectWindow();

        Selection.activeObject = asset;
    }

    [MenuItem("Assets/Create/New EnemyGroup")]
    public static void CreateEnemyGroupData()
    {
        EnemyGroup asset = ScriptableObject.CreateInstance<EnemyGroup>();

        AssetDatabase.CreateAsset(asset, "Assets/ScriptableObjects/EnemyGroups/EnemyGroup.asset");
        AssetDatabase.SaveAssets();

        EditorUtility.FocusProjectWindow();

        Selection.activeObject = asset;
    }


    [MenuItem("Assets/Create/Tower/New Tower")]
    public static void CreateTowerData()
    {
        TowerData asset = ScriptableObject.CreateInstance<TowerData>();

        AssetDatabase.CreateAsset(asset, "Assets/ScriptableObjects/Towers/Tower.asset");
        AssetDatabase.SaveAssets();

        EditorUtility.FocusProjectWindow();

        Selection.activeObject = asset;
    }

    [MenuItem("Assets/Create/Tower/New DamageBuffTower")]
    public static void CreateDamageBuffTowerData()
    {
        DamageBuffTowerData asset = ScriptableObject.CreateInstance<DamageBuffTowerData>();

        AssetDatabase.CreateAsset(asset, "Assets/ScriptableObjects/Towers/DamageBuffTower.asset");
        AssetDatabase.SaveAssets();

        EditorUtility.FocusProjectWindow();

        Selection.activeObject = asset;
    }

    [MenuItem("Assets/Create/New Multiplier")]
    public static void CreateMultiplierData()
    {
        MultiplierObject asset = ScriptableObject.CreateInstance<MultiplierObject>();

        AssetDatabase.CreateAsset(asset, "Assets/ScriptableObjects/MultiplierObject/MultiplierObject.asset");
        AssetDatabase.SaveAssets();

        EditorUtility.FocusProjectWindow();

        Selection.activeObject = asset;
    }

    [MenuItem("Assets/Create/Missions/New Mission")]
    public static void CreateMissionObject()
    {
        MissionObject asset = ScriptableObject.CreateInstance<MissionObject>();

        AssetDatabase.CreateAsset(asset, "Assets/ScriptableObjects/Missions/MissionObject.asset");
        AssetDatabase.SaveAssets();

        EditorUtility.FocusProjectWindow();

        Selection.activeObject = asset;
    }

    [MenuItem("Assets/Create/Missions/New Enemy Stat Mission")]
    public static void CreateMissionEnemyStatModifierObject()
    {
        EnemyStatModifierMission asset = ScriptableObject.CreateInstance<EnemyStatModifierMission>();

        AssetDatabase.CreateAsset(asset, "Assets/ScriptableObjects/Missions/MissionEnemyStatModifier.asset");
        AssetDatabase.SaveAssets();

        EditorUtility.FocusProjectWindow();

        Selection.activeObject = asset;
    }

    [MenuItem("Assets/Create/Reward/Gold Reward")]
    public static void CreateGoldRewardObject()
    {
        GoldRewardObject asset = ScriptableObject.CreateInstance<GoldRewardObject>();

        AssetDatabase.CreateAsset(asset, "Assets/ScriptableObjects/Rewards/GoldReward.asset");
        AssetDatabase.SaveAssets();

        EditorUtility.FocusProjectWindow();

        Selection.activeObject = asset;
    }

    [MenuItem("Assets/Create/Reward/Stat Reward")]
    public static void CreateStatRewardObject()
    {
        StatModifierReward asset = ScriptableObject.CreateInstance<StatModifierReward>();

        AssetDatabase.CreateAsset(asset, "Assets/ScriptableObjects/Rewards/StatReward.asset");
        AssetDatabase.SaveAssets();

        EditorUtility.FocusProjectWindow();

        Selection.activeObject = asset;
    }

    [MenuItem("Assets/Create/StatModifiers/New Tower Stat Reward")]
    public static void CreateTowerStatAddRewardObject()
    {
        TowerStatModifier asset = ScriptableObject.CreateInstance<TowerStatModifier>();

        AssetDatabase.CreateAsset(asset, "Assets/ScriptableObjects/StatModifiers/TowerAddStatReward.asset");
        AssetDatabase.SaveAssets();

        EditorUtility.FocusProjectWindow();

        Selection.activeObject = asset;
    }

    [MenuItem("Assets/Create/StatModifiers/Enemy Stat Modifier")]
    public static void CreateEnemytatObject()
    {
        EnemyStatModifier asset = ScriptableObject.CreateInstance<EnemyStatModifier>();

        AssetDatabase.CreateAsset(asset, "Assets/ScriptableObjects/StatModifiers/EnemyStatModifier.asset");
        AssetDatabase.SaveAssets();

        EditorUtility.FocusProjectWindow();

        Selection.activeObject = asset;
    }
}