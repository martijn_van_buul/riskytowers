﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Enemy : MonoBehaviour
{
    public float maxHealth = 10;

    public float health = 10;
    public float speed = 5;
    public int goldValue = 1;

    List<Vector3> path;
    bool started;
    public int pathIndex;

    private Color startColor;

    [SerializeField]
    Transform healthBar;
    [SerializeField]
    GameObject damageTextPrefab;

    public delegate void EnemyDeath();
    public EnemyDeath onEnemyDeath;


    public delegate void EnemyPassed(Enemy enemy);
    public EnemyPassed onEnemyPassed;

    private SpriteRenderer myRenderer;

    public void Initiate(EnemyData data, List<Vector3> path, float modifier = 1f)
    {
        maxHealth = health = data.MaxHealth * modifier;
        speed = data.Speed;
        goldValue = data.GoldValue;

        started = true;
        this.path = path;
        pathIndex = 0;

        myRenderer = GetComponent<SpriteRenderer>();
        startColor = myRenderer.color;

    }


    private void Update()
    {
        if (started)
        {
            transform.position = Vector3.MoveTowards(transform.position, path[pathIndex], speed * Time.deltaTime);
            if (transform.position == path[pathIndex])
            {
                pathIndex++;

                if(pathIndex == path.Count)
                {
                    started = false;


                    if (onEnemyPassed != null)
                        onEnemyPassed(this);
                }
            }
        }
    }

    public void Damage(float damage)
    {
        health = Mathf.Clamp(health - damage, 0, maxHealth);
        healthBar.transform.localScale = new Vector3(health / maxHealth, 1, 1);


        myRenderer.color = Color.white;
        myRenderer.DOKill();
        myRenderer.DOColor(startColor, 0.1f);
        transform.DOKill();
        transform.localScale = Vector3.one;
        transform.DOPunchScale(Vector3.one * 0.3f, 0.1f);

        SpawnDamageText(damage);

        if (health == 0)
        {
            if (onEnemyDeath != null)
                onEnemyDeath();

            EnemyManager.instance.DestroyEnemy(this);
        }
    }

    private void SpawnDamageText(float damage)
    {
        GameObject instantiatedDamageText = Instantiate(damageTextPrefab, transform.position, damageTextPrefab.transform.rotation, transform);
        instantiatedDamageText.transform.localPosition -= Vector3.up * 0.25f;
        instantiatedDamageText.transform.DOLocalMoveY(instantiatedDamageText.transform.position.y - 0.5f, 0.5f).SetEase(Ease.OutCirc);
        instantiatedDamageText.GetComponent<TMPro.TMP_Text>().text = damage.ToString();
    }
}
