﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenerateUITower : MonoBehaviour
{
    [SerializeField]
    GameObject UITowerPrefab;

    List<GameObject> UITowerObjects = new List<GameObject>();

    [SerializeField]
    List<TowerData> towerData;

    void Start()
    {
        Refresh(true);
    }

    public void Refresh(bool free = false)
    {
        if (!(UIManager.instance.goldAmount >= 2 || free))
            return;

        if(free)
            UIManager.instance.Refresh(0);
        else
            UIManager.instance.Refresh();


        int yPosition = -100;

        foreach (GameObject tower in UITowerObjects)
        {
            Destroy(tower);
        }

        UITowerObjects.Clear();

        for (int i = 0; i < 5; i++)
        {
            GameObject InstantiatedUITower = Instantiate(UITowerPrefab, transform.position, UITowerPrefab.transform.rotation, transform);
            InstantiatedUITower.GetComponent<RectTransform>().anchoredPosition = new Vector3(-100, yPosition, 0);
            InstantiatedUITower.GetComponentInChildren<UITower>().initialise(towerData[Random.Range(0, towerData.Count)]);
            yPosition += 100;
            UITowerObjects.Add(InstantiatedUITower);
        }
    }
}
