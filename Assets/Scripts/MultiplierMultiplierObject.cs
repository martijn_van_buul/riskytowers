﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MultiplierObject : ScriptableObject
{
    public GameObject Source;
    public float Multiplier;
}
