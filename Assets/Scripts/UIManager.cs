﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.EventSystems;
using DG.Tweening;

public class UIManager : MonoBehaviour
{
    public static UIManager instance;

    [SerializeField]
    TMP_Text goldText;
    [SerializeField]
    TMP_Text waveText;
    [SerializeField]
    TMP_Text healthText;

    [SerializeField]
    RectTransform infoRectTransform;
    [SerializeField]
    TowerData currentData;

    public int goldAmount = 10;
    public int waveCount = 1;
    private int health = 10;

    bool isHovering = false;

    private void Awake()
    {
        instance = this;
    }

    void Start()
    {
        infoRectTransform.localScale = Vector3.zero;
        EnemyManager.instance.onEnemyDeath += EnemyDied;
        EnemyManager.instance.onWaveCompleted += WaveComplete;
        EnemyManager.instance.onEnemyPassed += EnemyPassed;

        TowerManager.instance.onTowerRemoved += TowerRemoved;
    }

    private void EnemyDied(Enemy enemy)
    {
        goldAmount += enemy.goldValue;
        goldText.text = "Gold " + goldAmount;
    }

    private void TowerRemoved(Tower tower)
    {
        goldAmount += Mathf.FloorToInt(tower.Cost / 2);
        goldText.text = "Gold " + goldAmount;
    }


    private void WaveComplete()
    {
        waveCount++;

        waveText.text = "Wave " + waveCount;
    }

    private void EnemyPassed(Enemy enemy)
    {
        health--;

        healthText.DOKill();
        healthText.color = Color.red;
        healthText.DOColor(Color.white, 0.25f);

        healthText.text = "Health " + health;
    }


    public void BuyTower(int cost)
    {
        goldAmount -= cost;
        goldText.text = "Gold " + goldAmount;

        if (cost > 0)
        {
            goldText.DOKill();
            goldText.color = Color.red;
            goldText.DOColor(Color.white, 0.25f);
        }
    }

    public void Refresh(int cost = 2)
    {
        goldAmount -= cost;
        goldText.text = "Gold " + goldAmount;

        if (cost > 0)
        {
            goldText.DOKill();
            goldText.color = Color.red;
            goldText.DOColor(Color.white, 0.25f);
        }
    }

    public void HoveredUITower(UITower tower)
    {
        if ((tower != null && !isHovering) || (tower != null && tower.MyData != currentData))
        {
            isHovering = true;
            currentData = tower.MyData;

            infoRectTransform.GetChild(1).GetComponent<TMP_Text>().text = currentData.GetDescription();

            infoRectTransform.DOScale(1, 0.2f).SetEase(Ease.OutBounce);
        }
        else if(tower == null && isHovering)
        {
            infoRectTransform.DOScale(0, 0.2f).SetEase(Ease.InCirc);

            isHovering = false;
            currentData = null;
        }

    }
}
