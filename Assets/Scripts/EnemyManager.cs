﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyManager : MonoBehaviour
{
    public List<Wave> waves;

    public static EnemyManager instance;

    public List<Enemy> Enemies = new List<Enemy>();

    [SerializeField]
    GameObject enemyPrefab;

    List<GameObject> pathTiles;
    List<Vector3> path = new List<Vector3>();

    int currentWaveKilled = 0;
    int currentWaveTotal = 0;

    public delegate void EnemyDeath(Enemy enemy);
    public EnemyDeath onEnemyDeath;

    public delegate void WaveCompleted();
    public WaveCompleted onWaveCompleted;

    public delegate void EnemyPassed(Enemy enemy);
    public EnemyPassed onEnemyPassed;

    int waveCount = 0;


    private void Awake()
    {
        instance = this;
    }

    public void SetPath(List<GameObject> tiles)
    {
        pathTiles = tiles;

        foreach (GameObject tile in tiles)
            path.Add(tile.transform.position);
    }

    public void DestroyEnemy(Enemy enemy)
    {
        Enemies.Remove(enemy);

        if (onEnemyDeath != null)
            onEnemyDeath(enemy);

        Destroy(enemy.gameObject);
        currentWaveKilled++;

        if (currentWaveKilled == currentWaveTotal)
            WaveComplete();
    }

    public IEnumerator SpawnWave(float delay = 0f)
    {
        Wave wave = waves[waveCount % waves.Count];
        waveCount++;

        yield return new WaitForSeconds(delay);
        Enemies.Clear();
        currentWaveTotal = wave.EnemyGroups.Count;

        foreach (EnemyData data in wave.EnemyGroups[0].Enemies)
        {
            GameObject instantiatedEnemy = Instantiate(enemyPrefab, pathTiles[0].transform.position, enemyPrefab.transform.rotation);
            Enemy enemy = instantiatedEnemy.GetComponent<Enemy>();
            enemy.onEnemyPassed += EnemyPasses;

            enemy.Initiate(data, path, Mathf.Pow(1.2f, waveCount));
            Enemies.Add(enemy);

            instantiatedEnemy.transform.SetParent(transform);

            yield return new WaitForSeconds(wave.TimeBetween);
        }
    }

    private void WaveComplete()
    {
        currentWaveKilled = 0;

        if (onWaveCompleted != null)
            onWaveCompleted();

        StartCoroutine(SpawnWave(3f));
    }

    private void EnemyPasses(Enemy enemy)
    {
        if (onEnemyPassed != null)
            onEnemyPassed(enemy);

        enemy.Damage(enemy.maxHealth);
    }

    public Wave GetCurrentWave()
    {
        return waves[waveCount];
    }
}
