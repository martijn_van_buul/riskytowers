﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum E_EnemyVariables
{
    health,
    speed,
    goldValue,
}

public class EnemyData : ScriptableObject
{
    public float MaxHealth;
    public float Speed;
    public int GoldValue = 1;
    public Sprite EnemySprite;
    public Color EnemyColor;
}
