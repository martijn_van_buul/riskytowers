﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum E_MissionDifficulty
{
    Deadly,
    Dangerous,
    Challenging,
    Easy,
    Variable
}

public enum E_MissionType
{
    EnemyStatChange,
    NoDamage,
    NoBuilding
}

public class MissionObject : ScriptableObject
{
    public E_MissionDifficulty Difficulty;
    public E_MissionType Type;

    public string Description;
}
