﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletBehaviour : MonoBehaviour
{
    private Enemy target;
    private float damage;

    private float speed = 15;

    public void Initialise(Enemy target, float damage)
    {
        this.target = target;
        this.damage = damage;

        target.onEnemyDeath += EnemyDied;
    }

    private void Update()
    {
        transform.position = Vector3.MoveTowards(transform.position, target.transform.position, speed * Time.deltaTime);

        if(Vector3.Distance(transform.position,target.transform.position) < 0.01f)
        {
            target.onEnemyDeath -= EnemyDied;
            target.Damage(damage);
            Destroy(gameObject);
        }
    }

    private void EnemyDied()
    {
        target.onEnemyDeath -= EnemyDied;
        Destroy(gameObject);
    }
}
